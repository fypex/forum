<div style="padding-top: 100px" class="container-fluid">
    <h4 class="block-title"><a>Настройки сайта</a></h4>
    <div class="row">
        <div class="col-md-12">
            <div class="card custom-card">
                <div class="card-body">
                    <h4 class="card-title"><a>Сайт</a></h4>
                    <form id="site-settings-form" method="POST">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-md-4 mt-2">
                                <input class="form-control" type="text" name="site" placeholder="Название сайта">
                            </div>
                            <div class="col-md-4 mt-2">
                                <input class="form-control" type="text" name="keywords" placeholder="Ключевые слова">
                            </div>
                            <div class="col-md-4 mt-2">
                                <input class="form-control" type="text" name="description" placeholder="Описание">
                            </div>
                        </div>

                    </form>

                    <a id="site-settings-button" class="btn btn-save">Сохранить</a>

                </div>
            </div>
        </div>
    </div>
</div>