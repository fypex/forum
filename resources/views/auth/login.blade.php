@extends('layouts.index_header')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="container">
                        <div class="auth-form">

                            <form action="{{ route('login') }}" method="POST">
                                {{ csrf_field() }}
                                <div class="mt-3">
                                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                        <div class="col-md-12 px-0">
                                            @if ($errors->has('email'))
                                                <div class="help-block-wrapper">
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('email') }}</strong>
                                                    </span>
                                                </div>
                                            @endif
                                            <input placeholder="Почта" id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" autofocus>
                                        </div>
                                    </div>
                                </div>

                                <div class="mt-3">
                                    <div class="mb-0 form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                        <div class="col-md-12 px-0">
                                            @if ($errors->has('password'))
                                                <div class="help-block-wrapper">
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('password') }}</strong>
                                                    </span>
                                                </div>
                                            @endif
                                            <input placeholder="Пароль" id="password" type="password" class="form-control" name="password">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 pl-4 mt-2">
                                    <input type="checkbox" class="custom-control-input" id="defaultUnchecked" name="remember" {{ old('remember') ? 'checked' : '' }}>
                                    <label class="custom-control-label" for="defaultUnchecked">Запомнить меня</label>
                                </div>
                                <div class="col-md-12 px-0">
                                    <button type="submit" class="btn btn-primary w-100 m-0 mt-2 auth-button">
                                        Войти
                                    </button>
                                </div>
                                <div class="row">
                                    <div class="col-6 px-0 text-left pt-2 pl-3">
                                        <a class="" href="{{ route('password.request') }}">Забыл пароль?</a>
                                    </div>
                                    <div class="col-6 px-0 text-right pt-2 pr-3">
                                        <a class="" href="{{ route('register')  }}">Нет аккаунта?</a>
                                    </div>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
