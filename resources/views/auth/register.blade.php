@extends('layouts.index_header')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="container">
                        <div class="auth-form">

                            <form action="{{ route('register') }}" method="POST">
                                {{ csrf_field() }}
                                <div class="mt-3">
                                    @if ($errors->has('name'))
                                        <div class="help-block-wrapper">
                                            <span class="help-block">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                        </div>
                                    @endif
                                    <input id="name" placeholder="Логин" type="text" class="form-control" name="name" value="{{ old('name') }}"  autofocus>
                                </div>
                                <div class="mt-3">
                                    @if ($errors->has('email'))
                                        <div class="help-block-wrapper">
                                            <span class="help-block">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        </div>
                                    @endif
                                    <input id="email" placeholder="Почта" type="email" class="form-control" name="email" value="{{ old('email') }}">
                                </div>
                                <div class="mt-3">
                                    @if ($errors->has('password'))
                                        <div class="help-block-wrapper">
                                            <span class="help-block">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                        </div>
                                    @endif
                                    <input id="password" placeholder="Пароль" type="password" class="form-control" name="password">
                                </div>
                                <div class="mt-3">
                                    @if ($errors->has('password'))
                                        <div class="help-block-wrapper">
                                            <span class="help-block">
                                                <strong>{{ $errors->first('password_confirmation') }}</strong>
                                            </span>
                                        </div>
                                    @endif
                                    <input id="password-confirm" placeholder="Пароль" type="password" class="form-control" name="password_confirmation">
                                </div>

                                <div class="col-md-12 px-0">
                                    <button type="submit" class="btn btn-primary w-100 m-0 mt-2 auth-button">
                                        Создать аккаунт
                                    </button>
                                </div>
                                <div class="col-md-12 px-0 text-right pt-2">
                                    <a class="" href="{{ route('login')  }}">Есть аккаунт?</a>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
