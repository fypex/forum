$('#site-settings-button').click(function() {
    var data = $('#site-settings-form').serializeArray();
    $.ajax({
        url: '/admin/settings/site/set',
        type: 'POST',
        dataType : 'JSON',
        data: data,
        success: function(data) {
            toastr.success(data.message);
        },
        error: function (data) {
            toastr.error(data.responseText);
        }
    });
});