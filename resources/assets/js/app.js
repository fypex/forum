$('.set-ru').click(function() {
    var data = $('#id-формы').serializeArray();
    $.ajax({
        url: '/lang/ru',
        type: 'GET',
        dataType : 'JSON',
        data: data,
        success: function(data) {
            if (data.message == 'set')
            {
                location.reload(true);
            }
        }
    });
});

$('.set-en').click(function() {
    var data = $('#id-формы').serializeArray();
    $.ajax({
        url: '/lang/en',
        type: 'GET',
        dataType : 'JSON',
        data: data,
        success: function(data) {
            if (data.message == 'set')
            {
                location.reload(true);
            }
        }
    });
});