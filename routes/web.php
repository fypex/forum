<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Auth::routes();

Route::get('/dashboard', 'DashboardController@index')->name('dashboard');


Route::group(['prefix' => 'admin', 'namespace' => 'Admin','middleware'=>['role','auth']],function (){

    Route::get('/', 'AdminController@admin')->name('admin.index');

});

Route::group(['prefix' => 'admin/settings', 'namespace' => 'Settings','middleware'=>['role','auth']],function (){

    Route::post('/site/set', 'SiteController@update');

});