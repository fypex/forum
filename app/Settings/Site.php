<?php

namespace App\Settings;

use Illuminate\Database\Eloquent\Model;

class Site extends Model
{

    protected $table = 'settings_site';

    public $timestamps = false;

    protected $fillable = ['site', 'keywords', 'description'];
}
