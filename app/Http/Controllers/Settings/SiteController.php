<?php

namespace App\Http\Controllers\Settings;

use App\Settings\Site;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SiteController extends Controller
{


    public function update(Request $request, Site $site)
    {
        if(empty($request->get('site')))
        {
            return response('Заполни название сайта', 402);
        }
        if(empty($request->get('keywords')))
        {
            return response('Заполни ключевые слова', 402);
        }
        if(empty($request->get('description')))
        {
            return response('Заполни описание сайта', 402);
        }

        $site = $site->find(1);
        if (!empty($site))
        {
            $site->site = $request->get('site');
            $site->keywords = $request->get('keywords');
            $site->description = $request->get('description');
            $site->save();

        }else{
            $site = new Site;
            $site->site = $request->get('site');
            $site->keywords = $request->get('keywords');
            $site->description = $request->get('description');
            $site->save();
        }

        return response()->json([
            'status' => 'success',
            'message' => 'Обновлено'
        ]);

    }


}
