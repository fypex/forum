var mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js').version()
   .sass('resources/assets/sass/app.sass', 'public/css').version();

mix.js('resources/assets/js/a_app.js', 'public/js').version()
    .sass('resources/assets/sass/a_app.sass', 'public/css').version();
